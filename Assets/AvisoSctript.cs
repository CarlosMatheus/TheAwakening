﻿using UnityEngine;
using System.Collections;

public class AvisoSctript : MonoBehaviour {

	public AudioSource som1;
	public AudioSource som2;
	public AudioSource som3;
	public AudioSource som4;
	public AudioSource som5;

	public Transform Aviso1;
	public Transform Aviso2;
	public Transform Aviso3;

	public Transform Player;

	float t=0;
	int cont =1;

	public float delta = 4;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{


		if (
			(
			    Mathf.Abs (Player.position.x - Aviso1.position.x) <= delta
			    &&
			    Mathf.Abs (Player.position.y - Aviso1.position.y) <= delta
			    &&
			    Mathf.Abs (Player.position.z - Aviso1.position.z) <= delta
			) ||

			(
			    Mathf.Abs (Player.position.x - Aviso2.position.x) <= delta
			    && Mathf.Abs (Player.position.y - Aviso2.position.y) <= delta
			    && Mathf.Abs (Player.position.z - Aviso2.position.z) <= delta
			) ||

			(
			    Mathf.Abs (Player.position.x - Aviso3.position.x) <= delta
			    && Mathf.Abs (Player.position.y - Aviso3.position.y) <= delta
			    && Mathf.Abs (Player.position.z - Aviso3.position.z) <= delta
			)) 
		{
			
			t += Time.deltaTime;

			if (t > 40) {
				switch (cont) {
				case 1:
					som1.Play ();
					cont++;
					t = 0;
					break;
				case 2:
					som2.Play ();
					cont++;
					t = 0;
					break;
				case 3:
					som3.Play ();
					cont++;
					t = 0;
					break;
				case 4:
					som4.Play ();
					cont++;
					t = 0;
					break;
				case 5:
					som5.Play ();
					cont = 1;
					t = 0;
					break;
				}
			}



		}

	
	}

}

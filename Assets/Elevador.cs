﻿using UnityEngine;
using System.Collections;

public class Elevador : MonoBehaviour {

	public Transform Player;
	public float delta=2f;
	bool ok=false;
	bool som=true;
	float t = 0;
	Rigidbody corporigido;
	public float velocidadeDescida = 6;
	AudioSource audio;

	// Use this for initialization
	void Start () 
	{
		corporigido = GetComponent<Rigidbody> ();
		audio = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if (Mathf.Abs (Player.position.x - transform.position.x) < delta && Mathf.Abs (Player.position.y - transform.position.y) < delta && Mathf.Abs (Player.position.z - transform.position.z) < delta) 
		{
			ok = true;
		}

		if (ok == true) 
		{
			t = t + Time.deltaTime;


				corporigido.velocity = new Vector3 (0, -velocidadeDescida, 0);
				if(som==true)
				{
					audio.Play();
					som = false;
				}


		}
		
	}
}

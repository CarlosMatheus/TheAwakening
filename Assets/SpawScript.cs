﻿using UnityEngine;
using System.Collections;

public class SpawScript : MonoBehaviour 
{
	MonsterAttack monsterScript;

	public Transform player, monster;
	float t = 0;
	public float time = 180;
	public float delaytime=60;
	public float time1=180, time2=120, time3=60, time4=30;

	int x1,x2,x3,x4;

	//coordinates x,y of the area
	float[] vetor = new float[8];

	bool aux=true;

	// Use this for initialization
	void Start () 
	{
		monsterScript = GetComponent<MonsterAttack> ();
		//monsterScript.isSpawned = false;
		//monsterScript.activated = false;
	}

	// Update is called once per frame
	void Update ()
	{
		t += Time.deltaTime;

		if (!monsterScript.activated) { 					  //se ele estiver ativado esse script nao roda mais
															  //just for the first frame set the time
			for (; t > delaytime && aux; aux = false) {       //nao se esqueca de alterar os tempos pelos script da tocha
				time = time1;
				t = 0;

				print ("Monster delay: OK");
			}
			

			if (t > time) {
				int mod = modulo ();	// will tell you in which module the player are
				if (mod != 5)
					bixo (player.position.x, player.position.z, mod);
				t = 0;
			}

			if (monsterScript.isSpawned) { //verify if it has already been spawned 
				trocar (player.position.x, player.position.z);
				verify (player.position.x, player.position.z);
			}
		} else {
			

		
		}
	}

	int modulo()
	{
		if ((player.position.x > 0 && player.position.z > 0) && !(Mathf.Abs (player.position.x) < 2 && Mathf.Abs (player.position.z) < 2
		    && !((Mathf.Abs (player.position.x) > 1 && Mathf.Abs (player.position.x) < 2) &&
		    (Mathf.Abs (player.position.z) > 1 && Mathf.Abs (player.position.z) < 2))))
			return 1;
		else if ((player.position.x < 0 && player.position.z > 0) && !(Mathf.Abs (player.position.x) < 2 && Mathf.Abs (player.position.z) < 2
		         && !((Mathf.Abs (player.position.x) > 1 && Mathf.Abs (player.position.x) < 2) &&
		         (Mathf.Abs (player.position.z) > 1 && Mathf.Abs (player.position.z) < 2))))
			return 2;
		else if ((player.position.x < 0 && player.position.z < 0) && !(Mathf.Abs (player.position.x) < 2 && Mathf.Abs (player.position.z) < 2
		         && !((Mathf.Abs (player.position.x) > 1 && Mathf.Abs (player.position.x) < 2) &&
		         (Mathf.Abs (player.position.z) > 1 && Mathf.Abs (player.position.z) < 2))))
			return 3;
		else if ((player.position.x > 0 && player.position.z < 0) && !(Mathf.Abs (player.position.x) < 2 && Mathf.Abs (player.position.z) < 2
		         && !((Mathf.Abs (player.position.x) > 1 && Mathf.Abs (player.position.x) < 2) &&
		         (Mathf.Abs (player.position.z) > 1 && Mathf.Abs (player.position.z) < 2))))
			return 4;
		else
			return 5;
	}

	void bixo(float x, float y, int mod) //fazer a fucao exclusiva para cada quadrante
	{
		print("Monster entrou na funcao bixo");

		float[,,] coord = new float[9, 4, 2]; // o int representa a posicao onde se tem 0x 0y

		coord [0, 0, 0] =12;
		coord [0, 0, 1] =12;
		coord [0, 1, 0] =6;
		coord [0, 1, 1] =12;
		coord [0, 2, 0] = 6;
		coord [0, 2, 1] =13;
		coord [0, 3, 0] =12;
		coord [0, 3, 1] =13;

		coord [1, 0, 0] = 12; //00;
		coord [1, 0, 1] = 11;	//02;
		coord [1, 1, 0] = 6;//08
		coord [1, 1, 1] = 11;	//02;
		coord [1, 2, 0] = 6;//08
		coord [1, 2, 1] = 12;//01;
		coord [1, 3, 0] = 12;//00
		coord [1, 3, 1] = 12;//01;

		coord [2, 0, 0] = 12;  //00
		coord [2, 0, 1] = 10;	//03;
		coord [2, 1, 0] = 6 ; //08
		coord [2, 1, 1] = 10;	//03;
		coord [2, 2, 0] = 6 ; //08
		coord [2, 2, 1] =11;//02;
		coord [2, 3, 0] = 12;//00
		coord [2, 3, 1] =11;//02;

		coord [3, 0, 0] = 9; //0409;
		coord [3, 0, 1]=4;
		coord [3, 1, 0] = 4; //1009;
		coord [3, 1, 1]=4;
		coord [3, 2, 0] = 4; //1008;
		coord [3, 2,1 ]=5;
		coord [3, 3, 0] = 9; //0408;
		coord [3, 3, 1]=5;

		coord [4, 0, 0] = 3; //1010;
		coord [4, 0,1 ]=4;
		coord [4, 1, 0] = 2; //1110;
		coord [4, 1, 1]=4;
		coord [4, 2, 0] = 2; //1105;
		coord [4, 2, 1]=7;
		coord [4, 3, 0] = 3; //1005;
		coord [4, 3, 1]=7;

		coord [5, 0, 0] = 2; //1111;
		coord [5, 0, 1]=3;
		coord [5, 1, 0] = 1; //1211;
		coord [5, 1, 1]=3;
		coord [5, 2, 0] = 1; //1204;
		coord [5, 2, 1]=8;
		coord [5, 3, 0] = 2; //1104;
		coord [5, 3, 1]=8;

		coord [6, 0, 0] = 8; //0410;
		coord [6, 0, 1]=3;
		coord [6, 1, 0] = 3; //1110;
		coord [6, 1, 1]=3;
		coord [6, 2, 0] = 3; //1109;
		coord [6, 2, 1]=4;
		coord [6, 3, 0] = 8; //0409;
		coord [6, 3, 1]=4;

		coord [7, 0, 0] = 8; //0411;
		coord [7, 0, 1]=2;
		coord [7, 1, 0] = 2; //1211;
		coord [7, 1, 1]=2;
		coord [7, 2, 0] = 2; //1210;
		coord [7, 2, 1]=3;
		coord [7, 3, 0] = 8; //0410;
		coord [7, 3, 1]=3;

		coord [8, 0, 0] = 8;//0412;
		coord [8, 0, 1]=1;
		coord [8, 1, 0] = 2; //1212;
		coord [8, 1, 1]=1;
		coord [8, 2, 0] = 2; //1211;
		coord [8, 2, 1]=2;
		coord [8, 3, 0] = 8; //0411;
		coord [8, 3, 1]=2;


		float min = 1000f, dist;			//distacia minima para descidir em que area abrir
		int mini = 0, mini2 = 0, minj = 0;	//referencia a i e j pra a area i(de 1 a 9) e ponto j(de a a d)
		//int a, b = 0;					    //c e d 
		float l, m;							//l e m para os casos de limite de area
		bool dentro = false;            	//vai servir para verificar se ele se encontra dentro do local proibido

		if (mod == 1) {
			print("mod=1");
			l = 0;
			m = 0;
		} else if (mod == 2) {
			print("mod=2");
			l = -14;
			m = 0;
		} else if (mod == 3) {
			l = -14;
			m = -14;
		} else {
			l = 0;
			m = -14;
		}
			
		for (int i = 0; i < 9; i++) 
		{    									//achar a menor distacia
			for (int j = 0; j < 4; j++) 
			{
				coord [i, j, 0] += l;
				coord [i, j, 1] += m; 			// em que isso vai ja traduzir o modulo certo 

				dist = Mathf.Sqrt (Mathf.Pow ((x - coord [i, j, 0]), 2) + Mathf.Pow ((y - coord [i, j, 1]), 2));
				if (dist < min) 
				{
					mini = i;		//esse eh a area
					minj = j;		//esse eh o ponto
					min = dist;

				}
			}
		} 
		// agora ja tem a menor distancia e 
		//print("mini");
		//print(mini);
		//print("minj");
		//print(minj);
		//print("dist");
		//print(min);

		// verificar agora se o player esta dentro de uma area nao permitida
		for (int cont = 0; cont < 4; cont++)   
			//vetor ja eh variavel global
			//vetor com os valores novamente(valores em x,y)
		{
			vetor [2*cont] = coord [mini, cont,0];
			vetor [2*cont + 1] = coord [mini, cont,1]; //aqui estava todo o erro

			//print(vetor [cont]);
			//print(vetor [cont+1]);
		}
		print(vetor [0]);
		print(vetor [1]);
		print(vetor [2]);
		print(vetor [3]);
		print(vetor [4]);
		print(vetor [5]);
		print(vetor [6]);
		print(vetor [7]);


		print((vetor [0] > x && vetor [1] < y) && (vetor [2] < x && vetor [3] < y) && (vetor [4] < x && vetor [5] > y) && (vetor [6] > x && vetor [7] > y));

		//if the player are in the nor permited area
		if ((vetor [0] > x && vetor [1] < y) && (vetor [2] < x && vetor [3] < y) && (vetor [4] < x && vetor [5] > y) && (vetor [6] > x && vetor [7] > y)) 
		{

			print("esta em local proibido");

			min = 1000;
			for (int i = 0; i < mini; i++) 
			{    								//achar a menor distacia
				for (int j = 0; j < 4; j++) 
				{
					dist = Mathf.Sqrt (Mathf.Pow ((x - coord [i, j, 0]), 2) + Mathf.Pow ((y - coord [i, j, 1]), 2));
					if (dist < min) 
					{
						mini2 = i;
						minj = j;
						min = dist;
					}
				}
			}
				
			//separate in two parts just to ignore the inside area
			for (int i = mini + 1; i < 9; i++) 
			{    									//achar a menor distacia
				for (int j = 0; j < 4; j++) 
				{
					dist = Mathf.Sqrt (Mathf.Pow ((x - coord [i, j, 0]), 2) + Mathf.Pow ((y - coord [i, j, 1]), 2));
					if (dist < min) 
					{
						mini2 = i;
						minj = j;
						min = dist;
					}
				}
			}

			mini = mini2;

			print("mini");
			print(mini);
			print("minj");
			print(minj);
			print("dist");
			print(min);

		}

		for (int cont = 0; cont < 4; cont++)   //vetor ja eh variavel global
											    //vetor com os valores novamente(valores em x,y)
		{
			vetor [2*cont] = coord [mini, cont,0];
			vetor [2*cont + 1] = coord [mini, cont,1]; //aqui estava todo o erro

			//print(vetor [cont]);
			//print(vetor [cont+1]);
		}
		/*
		print(vetor [0]);
		print(vetor [1]);
		print(vetor [2]);
		print(vetor [3]);
		print(vetor [4]);
		print(vetor [5]);
		print(vetor [6]);
		print(vetor [7]);
		*/	
		float coX, coY;

		//para as coordenadas de onde o bixo vai ficar
		if( Mathf.Abs(vetor[0]-vetor[2])>Mathf.Abs(vetor[1]-vetor[7]))
		{
			print("horizontal");

			if (minj == 0 || minj == 3) //esse eh para o formato horizontal
			{
				print("horizontal1");
				coX = (vetor [4] + vetor [2]) / 2;
				coY = (vetor [5] + vetor [3]) / 2;

			} 
			else 
			{
				print("horizontal2");

				coX = (vetor [0] + vetor [6]) / 2;
				coY = (vetor [1] + vetor [7]) / 2;
			}
		}
		else
		{
			print("vertical");

		if (minj == 0 || minj == 1) //esse eh para o formato vertical
		{
				print("vertical1");
				coX = (vetor[4] + vetor[6])/2;
				coY = (vetor[5] + vetor[7])/2;
		} 
		else 
		{
			print("vertical2");
				coX = (vetor [0] + vetor [2]) / 2;
				coY = (vetor [1] + vetor [3]) / 2;
		}
		}

		//print("cox= ");
		//print(coX );
		//print ("coy=");
		//print(coY );


		//now we spaw the monster:
		monster.position=new Vector3(coX,-1.2f,coY);

		//now we activate it
		monsterScript.isSpawned=true;

		print("Monster saiu na funcao bixo");
		t = 0;
	}//end of function

	void trocar(float x, float y)
	{
		float coX, coY;

		if( Mathf.Abs(vetor[0]-vetor[2])>Mathf.Abs(vetor[1]-vetor[7]))
		{
			//print("horizontal");

				if(x>(vetor[0]+vetor[2])/2)
				{
				coX = (vetor [4] + vetor [2]) / 2;
				coY = (vetor [5] + vetor [3]) / 2;
				} 
			else 
			{
				//print("horizontal2");

				coX = (vetor [0] + vetor [6]) / 2;
				coY = (vetor [1] + vetor [7]) / 2;
			}
		}
		else
		{
			//print("vertical");

			if(y<(vetor[7]+vetor[1])/2) //esse eh para o formato vertical
			{
				//print("vertical1");
				coX = (vetor[4] + vetor[6])/2;
				coY = (vetor[5] + vetor[7])/2;
			} 
			else 
			{
				//print("vertical2");
				coX = (vetor [0] + vetor [2]) / 2;
				coY = (vetor [1] + vetor [3]) / 2;
			}
		}

		monster.position=new Vector3(coX,-1.2f,coY);

	}

	void verify (float x, float y)
	{
		if ((vetor [0] > x && vetor [1] < y) && (vetor [2] < x && vetor [3] < y) && (vetor [4] < x && vetor [5] > y) && (vetor [6] > x && vetor [7] > y))
			monsterScript.activated = true;
	}

	void verify2 (float x, float y) //para quando ja estiver ativado/atacando
	{
		if (!((vetor [0] > x && vetor [1] < y) && (vetor [2] < x && vetor [3] < y) && (vetor [4] < x && vetor [5] > y) && (vetor [6] > x && vetor [7] > y)))
			monsterScript.activated = false;
	}
}
﻿using UnityEngine;
using System.Collections;

public class Praia_Puzzle1_Sensor : MonoBehaviour {

    void OnCollisionEnter (Collision colisao)
    {
        if (colisao.gameObject.tag == "Player"){
            GetComponent<Rigidbody>().isKinematic = false;
        }
    }

}

﻿using UnityEngine;
using System.Collections;

public class lightIntensity : MonoBehaviour {

	float t=0;

	Light light;

	// Use this for initialization
	void Start () {
	
		light = GetComponent<Light> ();

	}
	
	// Update is called once per frame
	void Update () 
	{
	t += Time.deltaTime;
	
		light.intensity = 4 + 2*Mathf.Cos ((2*Mathf.PI/5)* t);
	
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class fade : MonoBehaviour {

	public float fadeInTime, fadeOutTime;
	public bool shouldFadeOut = false;
	private RawImage image;
	public bool shouldFade = true;
	public bool aux=false, aux2 = true;

	void Awake() {
		image = GetComponent<RawImage> ();
		image.rectTransform.localScale = new Vector3 (Screen.width, Screen.height, 1);
	}
		

	void Update() {
		if (aux) {
			setAgain ();
			aux = false;
		}
		if (shouldFade)
			FadeIn ();
		if (shouldFadeOut)
			FadeOut ();
	}

	void DoFadeIn () 
	{
		image.color = Color.Lerp (image.color, Color.clear, fadeInTime * Time.deltaTime);
	}

	void FadeIn() 
	{
		image.enabled = true;
		if (aux2) {
			image.color = Color.black;
			aux2 = false;
		}
		DoFadeIn ();
		if (image.color.a <= 0.05f) {
			image.enabled = false;
			shouldFade = false;
		}
	}

	void DoFadeOut () 
	{
		image.color = Color.Lerp (image.color, Color.black, fadeOutTime * Time.deltaTime);
	}

	void FadeOut()
	{
		image.enabled = true;
		DoFadeOut ();
		if (image.color.a >= 0.95f) 
		{
			shouldFadeOut = false;
		}
	}
	void setAgain()
	{ 
		image.enabled = true;
		//isso vai deixar ele instataneamete preto
		image.color = Color.black;
	}

}

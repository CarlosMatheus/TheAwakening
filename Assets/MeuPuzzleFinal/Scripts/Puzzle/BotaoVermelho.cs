﻿using UnityEngine;
using System.Collections;

public class BotaoVermelho : MonoBehaviour {

	public bool apertado = false;
	public float velocidade;
	private bool finished = true;
	private Vector3 velocity;

	public IEnumerator GoDown() {
		while (transform.position.y >= -0.15) {
			finished = false;
			transform.position -= velocity * Time.deltaTime;
		}
		yield return null;
		finished = true;
	}

	public IEnumerator GoUp() {
		while (transform.position.y < 0)
			transform.position += velocity * Time.deltaTime;
		yield return null;
	}

	bool CheckTag (GameObject thing) {
		if (thing.tag == "CuboVermelho")
			return true;
		else
			return false;
	}

	void OnCollisionEnter (Collision colisao) {
		if (CheckTag (colisao.gameObject)) {
			GetComponent<AudioSource> ().Play ();
			apertado = true;
			if (transform.position.y >= 0)
				StartCoroutine (GoDown());
		}
	}

	void OnCollisionStay (Collision colisao) {
		if (colisao.gameObject == null || !CheckTag (colisao.gameObject)) {
			Debug.Log ("oi");
			apertado = false;
			if (finished) {
				StartCoroutine (GoUp ());
			}
		}
	}
		
	void OnCollisionExit (Collision colisao) {
		if (colisao.gameObject == null || CheckTag (colisao.gameObject)) {
			apertado = false;
			if (finished) {
				StartCoroutine (GoUp ());
			}
		}
	}

	void Awake () {
		velocity = new Vector3 (0f, velocidade, 0f);
	}


}

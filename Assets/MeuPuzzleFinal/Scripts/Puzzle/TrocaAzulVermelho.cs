﻿using UnityEngine;
using System.Collections;

public class TrocaAzulVermelho : MonoBehaviour {

	public GameObject[] cubosVermelhos;
	public GameObject[] cubosAzuis;
	public bool change = false;
	private Color vermelho;
	private Color azul;

	void Awake() {
		cubosVermelhos = GameObject.FindGameObjectsWithTag ("CuboVermelho");
		cubosAzuis = GameObject.FindGameObjectsWithTag ("CuboAzul");

	}
	void Start() {
		foreach (GameObject cubo in cubosVermelhos)
			vermelho = cubo.GetComponent<Renderer> ().material.color;
		foreach (GameObject cubo in cubosAzuis)
			azul = cubo.GetComponent<Renderer> ().material.color;
	}
	
	
	void Update() {
		if (change) {
			foreach (GameObject cubo in cubosVermelhos) {
				cubo.tag = "CuboAzul";
				cubo.transform.localScale -= new Vector3 (0f, 0.1f, 0f);
				cubo.GetComponent<Renderer> ().material.color = azul;
			}
			foreach (GameObject cubo in cubosAzuis) {
				cubo.tag = "CuboVermelho";
				cubo.GetComponent<Renderer> ().material.color = vermelho;
			}
		} else {
			foreach (GameObject cubo in cubosVermelhos) {
				cubo.tag = "CuboVermelho";
				//cubo.transform.localScale = new Vector3 (2.5f, 2.4998f, 2.5f);
				cubo.GetComponent<Renderer> ().material.color = vermelho;
			}
			foreach (GameObject cubo in cubosAzuis) {
				cubo.tag = "CuboAzul";
				cubo.GetComponent<Renderer> ().material.color = azul;
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class BotaoMagenta : MonoBehaviour {

	public bool apertado = false;
	public float velocidade;
	private bool finished = true;
	private Vector3 velocity;

	public IEnumerator GoDown() {
		while (transform.position.y >= -0.15) {
			finished = false;
			transform.position -= velocity * Time.deltaTime;
		}
		yield return null;
		finished = true;
	}

	public IEnumerator GoUp() {
		while (transform.position.y < 0)
			transform.position += velocity * Time.deltaTime;
		yield return null;
	}

	bool CheckTag (GameObject thing) {
		if (thing.tag == "CuboMagenta")
			return true;
		else
			return false;
	}

	void OnCollisionEnter (Collision colisao) {
		if (CheckTag (colisao.gameObject)) {
			apertado = true;
			GetComponent<AudioSource> ().Play ();
			if (transform.position.y >= 0)
				StartCoroutine (GoDown());
		}
	}

	void OnCollisionExit (Collision colisao) {
		if (CheckTag (colisao.gameObject)) {
			apertado = false;
			if (finished) {
				Debug.Log ("oi");
				StartCoroutine (GoUp ());
			}
		}
	}

	void Awake () {
		velocity = new Vector3 (0f, velocidade, 0f);
	}
}

﻿using UnityEngine;
using System.Collections;

public class PortaAmarela : MonoBehaviour {

	public GameObject botaoAmarelo;
	public float velocity;
	//private Rigidbody _rigidbody;

	bool CheckButtons () {
		if (botaoAmarelo.GetComponent<BotaoAmarelo> ().apertado)
			return true;
		else
			return false;
	}

	void Awake () {
		//_rigidbody = GetComponent<Rigidbody> ();
		//_rigidbody.useGravity = false;
		//_rigidbody.isKinematic = true;
	}

	void Update () {
		if (CheckButtons ()) {
			if (transform.position.y <= 17.83)
				transform.position += new Vector3 (0f, velocity, 0f);
		} else if (transform.position.y > 12.5)
			transform.position -= new Vector3 (0f, 10 * velocity, 0f);
	}
}

﻿using UnityEngine;
using System.Collections;

public class Vidro : MonoBehaviour {

	void Update () {
		if (GetComponent<Renderer>().material.color.a >0.2f)
			GetComponent<Renderer> ().material.color = Color.Lerp(GetComponent<Renderer>().material.color, Color.clear, 1);
	}
}

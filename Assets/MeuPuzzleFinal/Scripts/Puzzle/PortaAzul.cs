﻿using UnityEngine;
using System.Collections;

public class PortaAzul : MonoBehaviour {

	public GameObject botaoAzul;
	public float velocity;
	bool notLoop;
	//private Rigidbody _rigidbody;

	bool CheckButtons () {
		if (botaoAzul.GetComponent<BotaoAzul> ().apertado){
			if (notLoop) {
				GetComponent<AudioSource> ().Play ();
				notLoop = false;
			}
			return true;
		} else {
			notLoop = true;
			return false;
		}
	}

	void Awake () {
		//_rigidbody = GetComponent<Rigidbody> ();
		//_rigidbody.useGravity = false;
		//_rigidbody.isKinematic = true;
	}

	void Update () {
		if (CheckButtons ()) {
			if (transform.position.y <= 17.83)
				transform.position += new Vector3 (0f, velocity, 0f);
		} else if (transform.position.y > 12.5)
			transform.position -= new Vector3 (0f, 10 * velocity, 0f);
	}
}

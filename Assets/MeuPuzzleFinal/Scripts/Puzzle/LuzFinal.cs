﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LuzFinal : MonoBehaviour {

	public float velocity;
	public int numScene;
	public bool shouldIntensify = false;
	public float volumeSpeed;


	void Update () {
		GetComponent<Light> ().range += velocity;
		if (GetComponent<Light> ().range >= 100f)
			SceneManager.LoadScene (numScene);
		if (shouldIntensify) {
			GetComponent<AudioSource> ().volume += volumeSpeed * Time.deltaTime;
		}
	}
}

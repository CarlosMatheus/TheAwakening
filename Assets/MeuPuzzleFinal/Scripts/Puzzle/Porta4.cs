﻿using UnityEngine;
using System.Collections;

public class Porta4 : MonoBehaviour {

	public GameObject botaoVermelho;
	public GameObject botaoAzul;
	public GameObject botaoAmarelo;
	public float velocity;
	bool notLoop;
	//private Rigidbody _rigidbody;

	bool CheckButtons () {
		if (botaoVermelho.GetComponent<BotaoVermelho> ().apertado == true && botaoAzul.GetComponent<BotaoAzul> ().apertado == true && botaoAmarelo.GetComponent<BotaoAmarelo>().apertado == true){
			if (notLoop) {
				GetComponent<AudioSource> ().Play ();
				notLoop = false;
			}
			return true;
		} else {
			notLoop = true;
			return false;
		}
	}
	void Awake () {
		//_rigidbody = GetComponent<Rigidbody> ();
		//_rigidbody.useGravity = false;
		//_rigidbody.isKinematic = true;
	}

	void Update () {
		if (CheckButtons ()) {
			if (transform.position.y <= 17.83)
				transform.position += new Vector3 (0f, velocity, 0f);
		} else {
			if (transform.position.y > 12.5) {
				Debug.Log ("xau");
				transform.position -= new Vector3 (0f, 40 * velocity, 0f);
			}
		}
	}
}

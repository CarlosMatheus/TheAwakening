﻿using UnityEngine;
using System.Collections;

public class FechaPorta : MonoBehaviour {

	public GameObject porta1;
	public GameObject porta2;
	public GameObject porta3;
	public GameObject portaFinal;
	public GameObject luz;
	public GameObject fadeImage;
	private bool oneTime = true;

	void OnTriggerEnter (Collider col) {
		if (col.gameObject.tag == "Fechador01") {
			porta1.SetActive (true);
		} else if (col.gameObject.tag == "Fechador02") {
			porta2.SetActive (true);
		} else if (col.gameObject.tag == "Fechador03") {
			porta3.SetActive (true);
		} else if (col.gameObject.tag == "FechadorFinal") {
			portaFinal.SetActive (true);
		} else if (col.gameObject.tag == "TriggerDaLuz") {
			luz.GetComponent<LuzFinal> ().enabled = true;
			if (oneTime) {
				luz.GetComponent<AudioSource> ().Play();
				oneTime = false;
				luz.GetComponent<LuzFinal> ().shouldIntensify = true;
				fadeImage.GetComponent<FadeMeu> ().shouldFadeOut = true;
			}
		}
	}
}

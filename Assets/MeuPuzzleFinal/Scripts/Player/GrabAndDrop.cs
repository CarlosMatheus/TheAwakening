﻿using UnityEngine;
using System.Collections;

public class GrabAndDrop : MonoBehaviour {
	public float rayLenght;
	public bool shouldBlock;
	GameObject grabbedObject;
	private float objectMagnitute;
	private bool wait = true;
	private RaycastHit teste;
	private RigidbodyConstraints original;

	bool CheckIfCanGrab (GameObject thing) {
		if (thing.GetComponent<Rigidbody>() == null)
			return false;
		else
			return true;
	}

	GameObject GetMouseOnObject () {
		RaycastHit hit;
		Vector3 position = gameObject.transform.position;
		if (Physics.Raycast (position, Camera.main.transform.forward, out hit, rayLenght))
			return hit.collider.gameObject;
		return null;
	}

	void GrabObject (GameObject grabObject) {
		if (grabObject == null || CheckIfCanGrab(grabObject) == false)
			return;
		grabbedObject = grabObject;
		grabbedObject.layer = 2;
		objectMagnitute = grabbedObject.GetComponent<Collider> ().bounds.size.magnitude;
		//GetComponent<BoxCollider> ().center.x = (grabbedObject.GetComponent<Collider> ().bounds.size.magnitude / 2f) + GetComponent<CapsuleCollider> ().radius / 2f;
		//GetComponent<BoxCollider> ().center.y = 0f;
		//GetComponent<BoxCollider> ().center.z = 0f;
		//GetComponent<BoxCollider>().size.x = (grabbedObject.GetComponent<Collider> ().bounds.size.magnitude / 2f) + GetComponent<CapsuleCollider> ().radius / 2f;
		//GetComponent<BoxCollider> ().size.z = GetComponent<CapsuleCollider> ().radius;
		//GetComponent<BoxCollider> ().size.y = 0.1f;
		//grabbedObject.transform.parent = gameObject.transform;
		//grabbedObject.transform.position += new Vector3 (0f, 1.0f, grabbedObject.GetComponent<Collider>().bounds.size.magnitude);
		grabbedObject.GetComponent<Rigidbody> ().freezeRotation = true;
		grabbedObject.GetComponent<Rigidbody> ().isKinematic = true;
		grabbedObject.GetComponent<Rigidbody> ().useGravity = false;
	}

	void DropObject () {
		if (grabbedObject == null)
			return;
		grabbedObject.layer = 0;
		grabbedObject.GetComponent<Collider> ().enabled = true;
		GetComponent<BoxCollider> ().enabled = false;
		grabbedObject.GetComponent<Rigidbody> ().freezeRotation = false;
		grabbedObject.GetComponent<Rigidbody> ().isKinematic = false;
		grabbedObject.GetComponent<Rigidbody> ().useGravity = true;
		grabbedObject = null;
		wait = true;
		//Invoke ("Block2", 0.3f);
	}

	void Block() {
		grabbedObject.GetComponent<Collider> ().enabled = false;
		GetComponent<BoxCollider> ().enabled = true;
		GetComponent<BoxCollider> ().center = new Vector3 (0f, 0f, (objectMagnitute / 2f) + GetComponent<CapsuleCollider> ().radius / 2f);
		GetComponent<BoxCollider>().size = new Vector3 (GetComponent<CapsuleCollider>().radius*2f, 0.1f, (objectMagnitute + GetComponent<CapsuleCollider> ().radius));
	}

	/*void Block2() {
		grabbedObject.GetComponent<Rigidbody> ().freezeRotation = false;
		grabbedObject.GetComponent<Rigidbody> ().isKinematic = false;
		grabbedObject.GetComponent<Rigidbody> ().useGravity = true;
		grabbedObject = null;
		wait = true;
	}*/

	/*void Start () {
		original = GetComponent<Rigidbody> ().constraints;
	}*/

	void Update () {
		if (Input.GetButtonDown ("Interact")) {
			if (grabbedObject == null){
				if (GetMouseOnObject ().tag == "Trocador")
				GetMouseOnObject ().GetComponent<TrocaAzulVermelho> ().change = !GetMouseOnObject ().GetComponent<TrocaAzulVermelho> ().change;
				GrabObject (GetMouseOnObject ());
			}
			else
				DropObject ();
		}

		if (grabbedObject != null) {
			/*if (shouldBlock) {
				if (Physics.Raycast (transform.position, transform.forward, out teste, grabbedObject.GetComponent<Collider> ().bounds.size.magnitude)) {
					Debug.Log (teste.collider.ToString ());
					GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotation;
				} else {
					GetComponent<Rigidbody> ().constraints = original;
				}

				if (Physics.Raycast (transform.position, Vector3.Cross(transform.forward, Vector3.up), (GetComponent<CapsuleCollider>().radius) * 1.1f) || Physics.Raycast (transform.position, -Vector3.Cross(transform.forward, Vector3.up), (GetComponent<CapsuleCollider>().radius) * 1.1f)) {
					GetComponent<Mouse>().shouldClampX = true;
				} else {
					GetComponent<Mouse>().shouldClampX = false;
				}
			}*/

			grabbedObject.transform.position = transform.position + objectMagnitute * transform.forward;
			grabbedObject.transform.rotation = transform.rotation;
			if (wait) {
				Invoke ("Block", 1);
				wait = false;
			}
		} /*else
			GetComponent<Rigidbody> ().constraints = original;*/
	}

	void OnTriggerEnter (Collider col) {
		if (col.gameObject.tag == "Destruidor") {
			if (grabbedObject == null)
				return;
			Destroy (grabbedObject);
			grabbedObject = null;
			return;
		} 
	}

}

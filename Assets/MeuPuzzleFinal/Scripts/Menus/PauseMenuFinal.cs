﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseMenuFinal : MonoBehaviour {

	public GameObject pauseMenuCanvas;
	private int numScene;
	private int type;
	public GameObject player;

	public void TogglePauseMenu() {
		if (pauseMenuCanvas.GetComponent<Canvas> ().enabled) {
			pauseMenuCanvas.GetComponent<Canvas> ().enabled = false;
			player.GetComponent<Mouse> ().enabled = true;
			Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Locked;
			Time.timeScale = 1f;
		} else if (!pauseMenuCanvas.GetComponent<Canvas> ().enabled) {
			pauseMenuCanvas.GetComponent<Canvas> ().enabled = true;
			player.GetComponent<Mouse> ().enabled = false;
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.None;
			Time.timeScale = 0f;
		}
	}

	public void QuitGame () {
		Application.Quit();
	}

	public void Restart () {
		numScene = SceneManager.GetActiveScene ().buildIndex;
		Debug.Log (numScene.ToString ());
		if (numScene == 1 && player.GetComponent<Morte>().passou_checkpoint) 
			type = 1;
		if (numScene == 5)
			type = 2;
		else
			type = 3;
		if (type == 1) {
			player.transform.position = player.GetComponent<Morte> ().checkpoint;
		} else if (type == 2)
			SceneManager.LoadScene (6);
		else if (type == 3) {
			SceneManager.LoadScene (numScene);
		}
	}

	void Awake() {
		Time.timeScale = 1f;
	}
	void Update () {
		if(Input.GetButtonDown("esc"))
			TogglePauseMenu();
	}
}

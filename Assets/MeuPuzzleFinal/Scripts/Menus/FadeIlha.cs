﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FadeIlha : MonoBehaviour {

	public float fadeOutSpeed;
	public bool shouldFadeOut = false;
	public int numScene;
	private bool shouldFadeIn = true;
	RawImage image;

	void Awake(){
		image = GetComponent<RawImage> ();
		image.color = Color.black;
		image.enabled = true;
		image.rectTransform.localScale = new Vector3 (Screen.width, Screen.height, 1);
	}

	void Update () {
		if (shouldFadeIn)
			FadeIn ();

		if (shouldFadeOut)
			FadeOut ();
	}

	void FadeOut() {
		image.enabled = true;
		DoFade ();
		if (image.color.a > 0.95f) {
			image.color = Color.black;
			SceneManager.LoadScene (numScene);
		}
	}

	void DoFade() {
		image.color = Color.Lerp (image.color, Color.black, fadeOutSpeed * Time.deltaTime);
	}

	void FadeIn() {
		image.enabled = true;
		DoFadeIn ();
		if (image.color.a < 0.05f) {
			image.color = Color.clear;
			image.enabled = false;
			shouldFadeIn = false;
		}
	}

	void DoFadeIn() {
		image.color = Color.Lerp (image.color, Color.clear, fadeOutSpeed * Time.deltaTime);
	}
}

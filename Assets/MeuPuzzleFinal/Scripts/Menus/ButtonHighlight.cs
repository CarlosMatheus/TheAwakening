﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonHighlight : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public GameObject texto;
	private Color colorTexto;
	private Color colorBotao;

	public void OnPointerEnter (PointerEventData eventData) {
		texto.GetComponent<Text> ().color = colorBotao;
		GetComponent<Image> ().color = colorTexto;
	}

	public void OnPointerExit (PointerEventData eventData) {
		texto.GetComponent<Text> ().color = colorTexto;
		GetComponent<Image> ().color = colorBotao;
	}

	void Awake() {
		colorTexto = texto.GetComponent<Text> ().color;
		colorBotao = GetComponent<Image> ().color;
	}
}

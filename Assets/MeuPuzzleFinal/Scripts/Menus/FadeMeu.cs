﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FadeMeu : MonoBehaviour {

	public float fadeSpeed = 1.0f;
	public float fadeOutSpeed;
	public bool shouldFadeOut;
	private bool isStarting = true;
	RawImage image;

	void Awake(){
		image = GetComponent<RawImage> ();
		image.rectTransform.localScale = new Vector3 (Screen.width, Screen.height, 1);
	}

	void Start () {
		isStarting = true;
	}
	void Update () {
		if (isStarting)
			StartScene ();
		if (shouldFadeOut)
			EndScene ();
	}

	void FadeToClear () {
		image.color = Color.Lerp (image.color, Color.clear, fadeSpeed * Time.deltaTime);
	}
		

	void StartScene () {
		FadeToClear ();

		if (image.color.a <= 0.05f) {
			//image.color = Color.clear;
			image.enabled = false;
			isStarting = false;
		}
	}

	void FadeOut(){
		image.color = Color.Lerp (image.color, Color.white, fadeOutSpeed * Time.deltaTime);
	}

	void EndScene () {
		image.enabled = true;
		FadeOut ();
	}
}
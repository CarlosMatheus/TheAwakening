﻿using UnityEngine;
using System.Collections;

public class Modulo3Script : MonoBehaviour {

	Rigidbody corporigido;
	float t=0;

	// Use this for initialization
	void Start () {
		corporigido = GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void Update () {
		t = t + Time.deltaTime;

		corporigido.velocity = new Vector3 (0, -10 * (2 * Mathf.PI / 60f) * Mathf.Sin ((2 * Mathf.PI / 60f) * t + Mathf.PI*0.5f), 0);
	}
}

﻿using UnityEngine;
using System.Collections;

public class UnlockLabDoor : MonoBehaviour {

	public GameObject jogador;
	public GameObject door;

	Vector3 p_jogador;

	// Use this for initialization
	void Start () {
		p_jogador = jogador.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		p_jogador = jogador.transform.position;
		float distancia;
		distancia = Mathf.Sqrt( Mathf.Pow( (p_jogador.x - transform.position.x), 2f) + Mathf.Pow( (p_jogador.y - transform.position.y), 2f) + Mathf.Pow((p_jogador.z - transform.position.z), 2f));

		if (distancia <= 3)
			Destroy (door);
	}
}

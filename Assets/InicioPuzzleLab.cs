﻿using UnityEngine;
using System.Collections;

public class InicioPuzzleLab : MonoBehaviour {

	public GameObject imagem;
	float t=0;
	public float t_fade;
	public GameObject FCamera;
	private bool startAnimacao = false;

	// Use this for initialization
	void OnCollisionEnter(Collision coli)
	{ 

		if (coli.gameObject.tag == "InicioPuzzleLab") {

			GetComponent<FisMov2> ().enabled = false;
			GetComponent<Mouse> ().enabled = false;

			startAnimacao = true;

			imagem.GetComponent<FadeIlha> ().shouldFadeOut = true;
		}
	}

	void Update () {
		if (startAnimacao) {
			if (t < t_fade) {
				t += Time.deltaTime;
				if (FCamera.transform.eulerAngles.x > 0)
					FCamera.transform.Rotate (4 * Time.deltaTime, 0, 0);
			}
		}
	}

}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ReadPaper : MonoBehaviour
{
    public GameObject jogador;

    public AudioSource abrindo, fechando;

    public GameObject _canvas, Imagem;

    bool aberto = false;

    bool canvasBool = false;

    Vector3 p_jogador;

    //float delay = 0;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        p_jogador = jogador.transform.position;
        float distancia;
        distancia = Mathf.Sqrt( Mathf.Pow( (p_jogador.x - transform.position.x), 2f) + Mathf.Pow( (p_jogador.y - transform.position.y), 2f) + Mathf.Pow((p_jogador.z - transform.position.z), 2f));

		/*if (Mathf.Sqrt (Mathf.Pow ((point.transform.position.x - transform.position.x), 2f) + Mathf.Pow ((point.transform.position.y - transform.position.y), 2f) + Mathf.Pow ((point.transform.position.z - transform.position.z), 2f)) < 3f) 
		{
			Destroy (barreira);
		}*/



        if (distancia <= 3f)
        {


           // print("esta a uma distancia menor que a minima");
            if (!canvasBool)
            {
                _canvas.GetComponent<Canvas>().enabled = true;
                canvasBool = true;
            }


            if (Input.GetButtonDown("read") && !aberto)
            {
                Imagem.GetComponent<Image>().enabled = true;
                //print("abriu imagem");
                abrindo.Play();
                aberto = true;
            }

            else if (Input.GetButtonDown("read") && aberto)
            {
                //print("fechou imagem");
                Imagem.GetComponent<Image>().enabled = false;
                fechando.Play();
                aberto = false;
            }
        }
        else
        {
            if (canvasBool)
            {
                _canvas.GetComponent<Canvas>().enabled = false;
                canvasBool = false;
            }
            Imagem.GetComponent<Image>().enabled = false;
            aberto = false;
        }

        }
}

﻿using UnityEngine;
using System.Collections;

public class Camera3Script : MonoBehaviour {

	public bool aux = false;
	public GameObject camera1, camera3;


	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{

		if(aux)
		{
			camera1.GetComponent<Camera>().enabled = false;
			camera1.SetActive (false);
			camera3.SetActive (true);
			camera3.GetComponent<Camera>().enabled = true;
		}
		
	}
}

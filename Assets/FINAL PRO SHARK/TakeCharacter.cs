﻿using UnityEngine;
using System.Collections;

public class TakeCharacter : MonoBehaviour {

	public GameObject player;
	public GameObject player_camera;
	public GameObject fader;

	bool entrou = false;

	void OnCollisionEnter(Collision col)
	{
		col.transform.parent = gameObject.transform;
		player.GetComponent<FisMov2> ().enabled = false;
		player.GetComponent<Mouse> ().enabled = false;
		player_camera.transform.eulerAngles = new Vector3 (280,0,0);
		fader.GetComponent<FadeTower> ().shouldFadeOut = true;

		entrou = true;

	}

	void Update()
	{
		if(entrou == true)
		{
			transform.Translate( 0 , 20*Time.deltaTime , 0);
		}
	}

}
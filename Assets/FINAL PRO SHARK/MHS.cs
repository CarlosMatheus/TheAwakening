﻿using UnityEngine;
using System.Collections;

public class MHS : MonoBehaviour {

	public float frequency;
	public float amplitude;
	float t = 0f;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		t = t + Time.deltaTime;
		transform.Translate( -2*Mathf.PI*frequency*amplitude*Mathf.Sin(2*Mathf.PI*frequency*t)*Time.deltaTime , 0, 0);
	}
}

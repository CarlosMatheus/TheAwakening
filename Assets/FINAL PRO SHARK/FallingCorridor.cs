﻿using UnityEngine;
using System.Collections;

public class FallingCorridor : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody> ().useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision col)
	{

		GetComponent<Rigidbody> ().AddForce (0,0.7f,0);
	}
}

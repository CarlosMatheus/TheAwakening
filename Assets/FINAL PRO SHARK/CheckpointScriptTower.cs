﻿using UnityEngine;
using System.Collections;

public class CheckpointScriptTower : MonoBehaviour {
	public GameObject sceneManager;
	public GameObject water;


	void OnTriggerEnter(Collider c)
	{
		if (c.gameObject.tag == "cpoint_virgem") {
			sceneManager.GetComponent<Scene_Manager> ().position_player_tower = c.transform.position;
			sceneManager.GetComponent<Scene_Manager> ().position_water_tower = water.transform.position;
			c.gameObject.tag = "cpoint_n_virgem";
		}
	
	}

	void Awake() {
		sceneManager = GameObject.Find ("SceneManager");

		transform.position = sceneManager.GetComponent<Scene_Manager> ().position_player_tower;
		water.transform.position = sceneManager.GetComponent<Scene_Manager> ().position_water_tower;
	}
}

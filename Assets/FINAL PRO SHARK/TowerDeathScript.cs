﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TowerDeathScript : MonoBehaviour {

	public GameObject sceneManager;
	public GameObject water;
	public GameObject camera_level;

	void Awake()
	{
		sceneManager = GameObject.Find ("SceneManager");
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (water.transform.position.y > camera_level.transform.position.y) {
			Morreu ();
		}
	}

	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "DeathObject")
			Morreu();
	}

	void Morreu()
	{
		print ("morreu");
		SceneManager.LoadScene (6);
	}

}

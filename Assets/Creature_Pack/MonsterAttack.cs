﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MonsterAttack : MonoBehaviour {

	public Transform player;

	public bool activated = true, isSpawned=true;///////////; 
	bool isRunning=false, isAttacking = false, isClose = false, isClose2=false;
	bool aux1=true, aux3=true, auxHB=true, auxMS=true, auxMonsSpaw=true, auxMR=true, auxMonsSte=true, aux2=true;
	public float monsterVelocity = 5.0f, distance=2.5f, distance2=0.4f, tempoLimiteAtaque=5;
	int rotateNum=0;
	public AudioSource heartbeating, morteSound, monsterSpawned, monsterRun, monstersteps;
	public GameObject imagemDoFade;
	float t=0, tmorte=0, tSpaw=0;

	int direction;

	Rigidbody rigidbody;
	Animator animator;

	// Use this for initialization
	void Start ()
	{
		rigidbody = GetComponent<Rigidbody> ();
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () 
	{

		if (!aux2) //para matar
		{
			tmorte += Time.deltaTime;
			if (tmorte > 3)
			{
				print("tmorte>3");
				SceneManager.LoadScene (9);
			}
		}

		if (isSpawned) 
		{

			tSpaw += Time.deltaTime;
			
			if (auxMonsSpaw) 
			{
				monsterSpawned.Play ();
				auxMonsSpaw = false;
			}
				

			if (activated && !isAttacking) 	//will only work when activeted in the other script
			{ 	
				t += Time.deltaTime; //comeca a contar

				if (rotateNum==0 || rotateNum==2)
					isClose = verifyf1 ();
				if (rotateNum==1 || rotateNum==3)
					isClose = verifyf2 ();

				if (!isAttacking)
					run ();

				if (isClose)
					attack ();

			}

			if (isAttacking)
				attack ();

			if (t > tempoLimiteAtaque || tSpaw>60) 
			{
				transform.position = new Vector3 (100, 100, 100);
				transform.Rotate (0, -90*rotateNum, 0);
				rotateNum = 0;
				activated = false;
				isSpawned = false;
				isAttacking = false;
				isRunning = false;
				isClose = false;
				isClose2 = false;
				auxHB=true;
				auxMS=true;
				auxMonsSpaw=true;
				auxMR=true;
				auxMonsSte=true;
				aux1 = true;
				aux3 = true;
				animator.SetBool ("isAttacking", false);
				animator.SetBool("isRunning", false);
				tSpaw=0;
				t = 0;
			}
		}
	}
		
	void run()
	{
		if (auxHB) 
		{
			heartbeating.Play ();
			auxHB = false;
		}
		if (auxMR) 
		{
			monsterRun.Play ();
			auxMR = false;
		}
		if (auxMonsSte) 
		{
			monstersteps.Play ();
			auxMonsSte = false;
		}
	
		for(;aux3;aux3=false)
			direction = directionf(); //choose which direction to move the monster
		
		isRunning = true;


		float moviment = monsterVelocity*wayf(direction); // choose the way of the moviment

		if (direction == 1)
			rigidbody.velocity = new Vector3 (moviment, 0, 0);
	    else
			rigidbody.velocity = new Vector3 (0, 0, moviment);

		animator.SetBool("isRunning", true);
	}

	int directionf()
	{
		float a = Mathf.Abs (transform.position.x - player.position.x);
		float b = Mathf.Abs (transform.position.z - player.position.z);

		if (a > b) 
		{//vai ter que seguir em x
			if (transform.position.x < player.position.x) 
			{
				rotateNum = 1;
				transform.Rotate (0, 90, 0);
			}
			else
			{
				rotateNum = 3;
				transform.Rotate (0, 270, 0);
			}

			return 1;
		}
		else{ //vai ter que se mover em z
			if (transform.position.z > player.position.z) 
			{
				rotateNum = 2;
				transform.Rotate (0, 180, 0);
			}
			
			return 2;
		}
	}

	int wayf(int direction)
	{
		if (direction == 1) {
			if (player.position.x > transform.position.x)
				return 1;
			else
				return -1;
		} else {
			if (player.position.z > transform.position.z)
				return 1;
			else
				return -1;
		}
	}

	bool verifyf1()
	{
		if (Mathf.Abs (player.position.x - transform.position.x) <= distance2
			&& Mathf.Abs (player.position.z - transform.position.z) <= distance) {

			return true;
		} else
			return false;
	}
	bool verifyf2()
	{
		if (Mathf.Abs (player.position.x - transform.position.x) <= distance
			&& Mathf.Abs (player.position.z - transform.position.z) <= distance2) {

			return true;
		} else
			return false;
	}

	void attack() //comecou a atacar ele vai morre, nao adianta.
	{
		t += Time.deltaTime; //continua contando
		tmorte += Time.deltaTime;

		monstersteps.Stop(); //stop the steps
		if(aux2)
		{
			imagemDoFade.GetComponent<fade> ().shouldFadeOut = true;
			aux2 = false;
		}

		if (auxMS) 
		{
			morteSound.Play ();
			auxMS=false;
		}

		isAttacking = true;
		activated = true;
		distance = 3.0f;
		animator.SetBool ("isAttacking", true);
	}
}
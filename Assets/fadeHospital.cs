﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class fadeHospital : MonoBehaviour {

	public float fadeTime, max=35, fadeTimeOut;

	float t=0;
	bool aux=true;

	public float t1, t2, t3, t4;
	public GameObject doctor;
	public GameObject omg;
	public GameObject awaken;


	RawImage rawimage;

	// Use this for initialization
	void Start () 
	{
		rawimage = GetComponent<RawImage> ();

		rawimage.rectTransform.localScale = new Vector3 (Screen.width, Screen.height, 1);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (aux) 
		{
			t += Time.deltaTime;
			rawimage.color = Color.Lerp (rawimage.color, Color.clear, fadeTime * Time.deltaTime);	

			if (t > max) 
			{
				aux = false;
			}
		}

		if (t > t1 && t < t2) {
			doctor.GetComponent<Text> ().enabled = true;
		}

		if (t > t2 && t < t3) {
			doctor.GetComponent<Text> ().enabled = false;
			omg.GetComponent<Text> ().enabled = true;
		}

		if (t > t3 && t < t4) {
			omg.GetComponent<Text> ().enabled = false;
			awaken.GetComponent<Text> ().enabled = true;
		}

		if (t > max) 
		{
			awaken.GetComponent<Text> ().enabled = false;
			rawimage.color= Color.Lerp (rawimage.color, Color.black, fadeTimeOut * Time.deltaTime);

			if (rawimage.color.a > 0.97)
				SceneManager.LoadScene (0);
		}




	}
}

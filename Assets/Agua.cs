﻿using UnityEngine;
using System.Collections;

public class Agua : MonoBehaviour {

	float t=0;
	Rigidbody corporigido;
	public float a = 11;
	public float f = 1;

	// Use this for initialization
	void Start () {
		corporigido = GetComponent<Rigidbody> ();
	
	}

	// Update is called once per frame
	void Update () {
		t= t + Time.deltaTime;

		corporigido.velocity = new Vector3 (0, -a * (2 * Mathf.PI / 60f) * Mathf.Sin ((2 * Mathf.PI / 60f) * t + Mathf.PI*f), 0);
	}
}

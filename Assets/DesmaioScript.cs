﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DesmaioScript : MonoBehaviour {

	float distancia;
	public GameObject jogador;
	public GameObject FCamera;
	public GameObject imagem;

	float t=0;
	public float t_fade;
	Vector3 p_jogador;

	// Use this for initialization
	void Start () {
		p_jogador = jogador.transform.position;
	}

	// Update is called once per frame
	void Update () {
		
		p_jogador = jogador.transform.position;

		distancia = Mathf.Sqrt( Mathf.Pow( (p_jogador.x - transform.position.x), 2f) + Mathf.Pow( (p_jogador.y - transform.position.y), 2f) + Mathf.Pow((p_jogador.z - transform.position.z), 2f));

		if (distancia < 3f) {
			jogador.GetComponent<Mouse> ().enabled = false;
			jogador.GetComponent<FisMov2> ().enabled = false;

			if (t<t_fade)
			{
			t += Time.deltaTime;
				if (FCamera.transform.eulerAngles.x>0)
					FCamera.transform.Rotate (4 * Time.deltaTime, 0, 0);
			//se der tempo a gente faz o desmaio
			}
			imagem.GetComponent<FadeIlha> ().shouldFadeOut = true;
		}
	}
}

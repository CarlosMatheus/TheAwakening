﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Morte : MonoBehaviour 
{
	public float max = 4;
	private bool aux1=true, aux2=false;
	private float valor1=0, valor2=0;
	private Rigidbody rigidbody;
	public int numScene;

	float debugger;

	public Vector3 checkpoint = new Vector3 (0, 0, 0);

	public bool passou_checkpoint = false;

	// Use this for initialization
	void Start () 
	{
		rigidbody = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (transform.position.y < 37.41322f && transform.position.y > -100.0f)
			mortefunc();
		if (deltavel () > max) {
			mortefunc ();
			Debug.Log (debugger.ToString ());
		}
	}

	void OnTriggerEnter(Collider c)
	{
		if (c.gameObject.tag == "IslandCheckpoint") {
			passou_checkpoint = true;
			checkpoint = transform.position;
		}
	}

	void mortefunc()
	{
		if (passou_checkpoint)
			transform.position = checkpoint;
		else
			SceneManager.LoadScene (numScene);
	}
	float deltavel()
	{
		//Debug.Log (ToString (rigidbody.velocity.y));
		if (aux1) 
		{
			valor1 = rigidbody.velocity.y;
			aux1 = false;
			aux2 = true;
			debugger = Mathf.Abs (valor1 - valor2);
			return (Mathf.Abs(valor1-valor2));
		}
		else
		{
			valor2 = rigidbody.velocity.y;
			aux1 = true;
			aux2 = false;
			debugger = Mathf.Abs (valor1 - valor2);
			return (Mathf.Abs(valor1-valor2));
		}
	}

}

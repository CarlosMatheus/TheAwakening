﻿using UnityEngine;
using System.Collections;

public class mapScript : MonoBehaviour 
{

	public AudioSource abrindo, fechando;

	public GameObject Texto, Imagem;

	bool aux= true;

	float delay=0;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		delay += Time.deltaTime;

		Texto.SetActive (false);

		if (delay > 10 && aux)
			Texto.SetActive (true);

		if (!Input.GetButton ("v"))
			Imagem.SetActive (false);
		else {
			aux = false;
			Imagem.SetActive (true);
		}

		if (Input.GetButtonDown ("v"))
			abrindo.Play ();

		if(Input.GetButtonUp("v"))
			fechando.Play ();
		
	}
}

﻿using UnityEngine;
using System.Collections;

public class Cartas : MonoBehaviour {

	public GameObject cameraInicial, cameraFinal;
	private Camera _cameraInicial, _cameraFinal;
	private float t = 0f;

	void Awake () {
		_cameraInicial = cameraInicial.GetComponent<Camera> ();
		_cameraFinal = cameraFinal.GetComponent<Camera> ();
		_cameraFinal.enabled = false;
		cameraFinal.GetComponent<AudioListener> ().enabled = false;
		_cameraInicial.enabled = true;
		cameraInicial.GetComponent<AudioListener> ().enabled = true;
	}

	void Update () {
		t += Time.deltaTime;
		if (t > 10 && t<15) {
			Indo ();
		}
		if (t > 15) {
			Voltando ();
		}
	}

	void Indo() {
		Debug.Log ("Trocandooo");
		_cameraInicial.enabled = false;
		_cameraFinal.enabled = true;
		GetComponent<FisMov> ().enabled = false;
		cameraInicial.GetComponent<AudioListener> ().enabled = false;
		GetComponent<Mouse> ().enabled = false;
		cameraFinal.GetComponent<AudioListener> ().enabled = true;
	}

	void Voltando() {
		Debug.Log ("Voltandooo");
		_cameraInicial.enabled = true;
		_cameraFinal.enabled = false;
		GetComponent<FisMov> ().enabled = true;
		cameraInicial.GetComponent<AudioListener> ().enabled = true; 
		GetComponent<Mouse> ().enabled = true; 
		cameraFinal.GetComponent<AudioListener> ().enabled = false; 
		}
}

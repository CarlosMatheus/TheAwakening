﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DeathMenu : MonoBehaviour {

	RawImage image;
	public bool show = false;

	void Awake(){
		image = GetComponent<RawImage> ();
		image.rectTransform.localScale = new Vector3 (Screen.width, Screen.height, 1);
		image.enabled = false;
	}

	void Update(){
		if (show) {
			image.enabled = true;
		}
	}
}

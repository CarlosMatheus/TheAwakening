﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

//Primeiro: O que é Canvas:
//			Canvas é basicamente um jeito de falar pro unity: "Ou, vou querer botar objetos de GUI aqui". Então todo elemento GUI tem que ser filho de um Canvas
//OBS: Eu não entendo muito bem como funcione, talvez tenha falado besteira. Então pesquise!!!

public class Menu : MonoBehaviour {

	//Referencia do Menu de "Deseja sair"
	public GameObject QuitMenu;

	//Referencia do botão de sair
	public GameObject quit;

	//Referencia do botão de jogar
	public GameObject play;

	void Start () {

		//Pegando referencias
		quit = GameObject.Find ("Quit");
		play = GameObject.Find ("Play");
		QuitMenu = GameObject.Find ("QuitMenu");

		//O menu de "Deseja Sair" comeca desativado, então para desativá-lo basta desligar o seu Canvas logo no início
		QuitMenu.GetComponent<Canvas> ().enabled = false;
	}

	//Funcão que será chamada caso o botão de Quit seja apertado
	public void onQuitPress() {
		//Abre o menu de "Deseja Sair"
		QuitMenu.GetComponent<Canvas> ().enabled = true;
		//Desliga o componente de botão e de texto do botão de play para que não seja possível apertá-lo nem vê-lo
		play.GetComponent<Button> ().enabled = false;
		play.GetComponent<Text> ().enabled = false;
		//Desliga o componente de botão e de texto do botão de quit para que não seja possível apertá-lo nem vê-lo
		quit.GetComponent<Button> ().enabled = false;
		quit.GetComponent<Text> ().enabled = false;
	}

	//Funcão que será chamada caso o botão de Play seja apertado
	public void onPlayPress() {
		//Comeca o level inicial
		//SceneManager.LoadScene ("Scene1");
	}

	//Funcão que será chamada caso o botão de No seja apertado
	public void onNoPress() {
		//Fecha o menu de "Deseja Sair"
		QuitMenu.GetComponent<Canvas> ().enabled = false;
		//Ativa de volta os componentes do botão de play
		play.GetComponent<Button> ().enabled = true;
		play.GetComponent<Text> ().enabled = true;
		//Ativa de volta os componentes do botão de quit
		quit.GetComponent<Button> ().enabled = true;
		quit.GetComponent<Text> ().enabled = true;
	}

	//Funcão que será chamada caso o botão de Yes seja apertado
	public void onYesPress() {
		//Fecha o jogo
		Application.Quit ();
	}
}

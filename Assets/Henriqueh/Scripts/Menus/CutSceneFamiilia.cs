﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CutSceneFamiilia : MonoBehaviour {

	public GameObject audioCut;
	public int numScene;

	void Awake() {
		GetComponent<RawImage> ().color = Color.white;
		GetComponent<RawImage>().rectTransform.localScale = new Vector3 (Screen.width, Screen.height, 1);
	}

	void Update () {
		if (!audioCut.GetComponent<AudioSource> ().isPlaying)
			SceneManager.LoadScene (numScene);
	}
}

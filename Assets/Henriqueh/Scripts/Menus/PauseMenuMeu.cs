﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PauseMenuMeu : MonoBehaviour
{
    //Referencia do Jogador
    public GameObject player;
    //Referencia do menu de "Deseja Sair"
    public GameObject quitmenu;
    //Referencia do botão de quit
    public GameObject quit;
    //Referencia do botão de resume
    public GameObject resume;
    //Referencia do botão de save
    public GameObject save;


    void Start()
    {
        //O menu de pause não deve comecar na tela, logo devemos desativá-lo inicalmente
        this.GetComponent<Canvas>().enabled = false;
        //Pegando referencias
        player = GameObject.Find("FirstPerson");
        quit = GameObject.Find("ExitGame");
        resume = GameObject.Find("ResumeGame");
        save = GameObject.Find("SaveGame");
        quitmenu = GameObject.Find("QuitMenu");
        //O menu de "Deseja Sair" comeca desativado, então para desativá-lo basta desligar o seu Canvas logo no início
        quitmenu.GetComponent<Canvas>().enabled = false;
    }

    //Funcão que será chamada caso o botão de Save seja apertado
    public void onSavePress()
    {
        //Save
    }

    //Funcão que será chamada caso o botão de Resume seja apertado
    public void onResumePress()
    {
        //A tela de puase deve ser desativada
        this.GetComponent<Canvas>().enabled = false;
        //Devemos avisar ao script do jogador que o menu será fechado, através da bool shouldclose
        player.GetComponent<Pause>().shouldclose = true;
    }

    //Funcão que será chamada caso o botão de Quit seja apertado
    public void onQuitPress()
    {
        //Desliga os componentes de botão e de texto do botão de resume para que não seja possível apertá-lo nem vê-lo
        resume.GetComponent<Button>().enabled = false;
        resume.GetComponent<Text>().enabled = false;
        //Desliga os componentes de botão e de texto do botão de quit para que não seja possível apertá-lo nem vê-lo
        quit.GetComponent<Button>().enabled = false;
        quit.GetComponent<Text>().enabled = false;
        //Desliga os componentes de texto do botão de save para que não seja possível apertá-lo nem vê-lo
        save.GetComponent<Text>().enabled = false;
        //Abre o menu de "Deseja Sair"
        quitmenu.GetComponent<Canvas>().enabled = true;
    }

    //Funcão que será chamada caso o botão de No seja apertado
    //Literalmente o contrário do onQuitPress
    public void onNoPress()
    {
        //Ativa de volta os componentes do botão de resume
        resume.GetComponent<Button>().enabled = true;
        resume.GetComponent<Text>().enabled = true;
        //Ativa de volta os componentes do botão de quit
        quit.GetComponent<Button>().enabled = true;
        quit.GetComponent<Text>().enabled = true;
        //Ativa de volta o componente do botão de save
        save.GetComponent<Text>().enabled = true;
        //Fecha o menu de "Deseja Sair"
        quitmenu.GetComponent<Canvas>().enabled = false;
    }

    //Funcão que será chamada caso o botão de Yes seja apertado
    public void onYesPress()
    {
        //Fechar o jogo
        Application.Quit();
    }

}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour {

	//Pegando referência
	public GameObject portal;

	public GameObject fader;

	public int SceneNumber;

	//Intuitivo(distância do player até o "portal"
	private float distance = 0;

	//Booleana que diz se o jogador está perto o suficiente do portal. Necessário para dizer quando ele pode teleportar
	private bool isclose = false;

	//Responsável pelas caracterśiticas do texto ("Aperte E para entrar")
	public GUIStyle style = new GUIStyle();

	//OnGUI funciona tipo como um update, mas com elementos GUI. Uma diferenca é que o OnGUI não se limita a apenas 1 chamada por frame
	void OnGUI()
	{
		//O que isso faz?
		//If: Se o jogador está perto, entrar no if
		//Dentro do if: GUI.box é um método que basicamente irá criar uma caixa na câmera(na layer mask dela, que pode estar ativado ou desativado).
		//              Essa caixa pode conter basicamente qualquer elemento GUI. Ou seja, através de um parâmetro de texture, é possível fazer essa
		//				caixa representar uma imagem. Se você passar uma string, ela irá representar uma caixa de texto, e por aí vai.
		//				No nosso caso, estou usando como uma caixa de texto e o parâmetro style irá mudar algumas características do texto. O parâmetro
		//				new rect serve como dimensão para a caixa.
		if (isclose) {	
			GUI.Box (new Rect (0, Screen.height/4, Screen.width, Screen.height), "Aperte E para entrar", style);
		}
	}

	// Use this for initialization
	void Start () {
		//Pegando referência. Se necessário pode retirar
		portal = GameObject.Find("Portal");

		//Serve para dizer onde o texto ficará na caixa. Nesse caso ele ficará no centro. Outras opcões: MiddleLeft, UpperRight,...
		style.alignment = TextAnchor.MiddleCenter;

		//Dizer o tamanho da fonte do texto
		style.fontSize = 30;

		//Dizer a cor do texto
		style.normal.textColor = Color.blue;
	}
	
	// Update is called once per frame
	void Update () {
		//Pegar a distância entre o portal e o jogador
		distance = Vector3.Distance(this.transform.position, portal.transform.position);
			//Debug.Log ("Distancia: " + distance.ToString());

		//Condicão para dizer se o jogador está perto ou não
		//OBS: "?" funciona como if e else
			isclose = (distance<20) ? true : false;

		//Checando se o jogador apertou o botão de teleporte perto do portal
			if ( isclose && Input.GetButtonDown("e")){
				Debug.Log ("Apertou o botao");

				//Mudanca de level
				//OBS: USAR UNITYENGINE.SCENEMANAGEMENT
			fader.GetComponent<Fade>().EndScene(SceneNumber);
			}
	}
}

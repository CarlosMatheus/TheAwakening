﻿using UnityEngine;
using System.Collections;

public class Mouse : MonoBehaviour {

	public float sensitivity = 50f;
	public float slerpTime = 1.0f;
	Quaternion cameraRotation;
	Quaternion playerRotation;
	public GameObject selfCamera;
	public float clampAngle = 80.0f;
	public float xRotation = 0.0f;
	public float yRotation = 0.0f;

	void Awake () {
		selfCamera = GameObject.FindGameObjectWithTag ("MainCamera");
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
	}

	void Update () {	
		CursorUpdate ();

		//Eixo X
		xRotation += Input.GetAxis ("Mouse X") * sensitivity;
		playerRotation = Quaternion.Euler (0f, xRotation, 0f);


		//Eixo Y
		yRotation -= Input.GetAxis ("Mouse Y") * sensitivity;
		yRotation = Mathf.Clamp (yRotation, -clampAngle, clampAngle);
		cameraRotation = Quaternion.Euler ( yRotation, 0f, 0f);

		//Interpolacao Cagada
		transform.localRotation = Quaternion.Lerp(transform.localRotation, playerRotation, 1.0f);
		selfCamera.transform.localRotation = Quaternion.Lerp (selfCamera.transform.localRotation, cameraRotation, 1.0f);
	}

	void CursorUpdate () {
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
	}

	public void OnTeleportEnter (float rotation) {
		xRotation = rotation;
	}

}

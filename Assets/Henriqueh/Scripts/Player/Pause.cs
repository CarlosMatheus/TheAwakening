﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
/*using UnityStandardAssets.Characters.FirstPerson;*/

public class Pause : MonoBehaviour {

	//variavel que vai decidir se o jogo ta pausado ou nao
	public bool ispaused = false;

	//gambiarra que eu fiz, mas para frente vou explicar
	bool wait;

	//referencia para acessar o menu em si
	public GameObject PauseMenu;

	//variavel que diz que o menu deve ser fechado. É um jeito de outros scripts fecharem o menu
	public bool shouldclose;




	//Coroutine que deixará o jogo pausado. Não sei se é o melhor método, então pode ser melhorado
	public IEnumerator PauseGame(){
		//Aviso de que o jogo está pausado no console
		Debug.Log ("Jogo Pausado");

		//Para fazer o cursor aparecer para que o jogador possa mexer no menu
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;

		//Gambiarra master:
		//Problema que estava tendo: Ao apertar o botão de pausar, essa coroutine iniciava imediatamente e, portanto, entrava no frame
		//							 que o botão de pause ainda estava sendo apertado. Sendo assim, imediatamente já entrava no if lá 
		//							 embaixo e despausava.
		//Solucão: Criei esse while de tal forma que cada vez que se pausa, entra-se nele exatamente 1 vez e no primeiro ciclo em que 
		//		   a coroutine é chamada. A única funcão desse while é como se fosse um break, fazendo com que a coroutine tenha que 
		//		   esperar um frame para poder comecar o próximo ciclo.
		while (wait) {
			//Gambiarra
			OnGui ();

			wait = false;
			yield return null;
		}

		//While que fica checando se o pause deve ser fechado ou não
		while(ispaused){
			if (Input.GetButtonDown ("esc") || shouldclose) {
				shouldclose = false;
				Debug.Log ("Jogo voltara");

				//Para o tempo voltar a correr
				Time.timeScale = 1f;

				//Para voltar os comandos do jogador
				/*this.GetComponent <FirstPersonController>().enabled = true;*/
				this.GetComponent <FisMov> ().enabled = true;
				this.GetComponent <Mouse> ().enabled = true;
				ispaused = false;
				wait = true;

				//Desativando o menu
				PauseMenu.GetComponent<Canvas> ().enabled = false;
				yield break;
			}
			yield return null;
		}
	}





	void Start () {
		//Valores iniciais
		ispaused = false;
		wait = true;
		shouldclose = false;

		//Pegando referência
		PauseMenu = GameObject.Find ("Pause");

	}
	

	void Update () {
		//Se entrar no if, o jogo pausará
		if (!ispaused && Input.GetButtonDown ("esc")) {
			ispaused = true;
			OnGui ();

			//Desativando os controles do jogador
			/*this.GetComponent <FirstPersonController>().enabled = false;*/
			this.GetComponent <FisMov> ().enabled = false;
			this.GetComponent <Mouse> ().enabled = false;

			//Parando o tempo
			Time.timeScale = 0f;
			Debug.Log ("Jogo pausara");

			//Ativando o menu
			PauseMenu.GetComponent<Canvas> ().enabled = true;
			StartCoroutine (PauseGame());
		}
	}

	//Como eu não estava conseguindo fazer o cursor ficar visível, saí testando tudo.
	void OnGui() {
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.None;
		
	}
		
}

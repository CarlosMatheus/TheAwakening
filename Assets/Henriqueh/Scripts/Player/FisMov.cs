﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Rigidbody))]

public class FisMov2 : MonoBehaviour {

	[SerializeField] private AudioClip jumpAudio;
	[SerializeField] private AudioClip fallAudio;

	public float speed;
	public float normalSpeed = 5.0f;
	public float sprintSpeed = 7.0f;
	public float crouchSpeed = 3.0f;
	public float gravity = 10.0f;
	public float jumpHeight = 1.0f;
	public float crouchHeight = 1.3f;
	public float normalHeight = 2.0f;
	public float maxFallSpeed = 15.0f;
	private float rayLenght;
	private bool grounded = false;
	private bool isCrouched = false;
	private bool isJumping = false;
	private bool check;
	RaycastHit hit;
	Rigidbody selfRigidbody;
	CapsuleCollider selfCollider;
	AudioSource selfAudioSource;

	void Awake () {
		selfCollider = GetComponent<CapsuleCollider> ();
		selfRigidbody = GetComponent<Rigidbody> ();
		selfAudioSource = GetComponent<AudioSource> ();
		selfRigidbody.freezeRotation = true;
		selfRigidbody.useGravity = false;
	}

	void Start() {
		selfCollider.height = normalHeight;
		speed = normalSpeed;
	}
		
	void FixedUpdate () {
		CheckIfWatered ();
		grounded = CheckIfGrounded ();
		speed = normalSpeed;
		//Caso ele esteja no chão
		if (grounded) {
			if (Input.GetButton ("sprint") && !isCrouched) {
				speed = sprintSpeed;
			}
			
			if (Input.GetButton ("crouch")) {
				speed = crouchSpeed;
				selfCollider.height = crouchHeight;
				isCrouched = true;
			} else {
				selfCollider.height = normalHeight;
				isCrouched = false;
			}

			Vector3 velocidadeNova = new Vector3 (Input.GetAxis ("Horizontal"), 0, Input.GetAxis ("Vertical"));
			velocidadeNova = transform.TransformDirection (velocidadeNova);
			velocidadeNova *= speed;

			Vector3 velocidadeAntiga = selfRigidbody.velocity;
			Vector3 mudancaVelocidade = velocidadeNova - velocidadeAntiga;
			mudancaVelocidade.x = Mathf.Clamp (mudancaVelocidade.x, -speed, speed);
			mudancaVelocidade.y = 0;
			mudancaVelocidade.z = Mathf.Clamp (mudancaVelocidade.z, -speed, speed);
			selfRigidbody.AddForce (mudancaVelocidade, ForceMode.VelocityChange);
			
			if (Input.GetButton ("jump")) {
				selfRigidbody.velocity = new Vector3 (selfRigidbody.velocity.x, JumpVelocity (), selfRigidbody.velocity.y);
				selfAudioSource.clip = jumpAudio;
				selfAudioSource.Play ();
				isJumping = true;
			}
		} 
		else 
		{
			Vector3 velocidadeNova = new Vector3 (Input.GetAxis ("Horizontal"), 0, Input.GetAxis ("Vertical"));
			velocidadeNova = transform.TransformDirection (velocidadeNova);
			velocidadeNova *= speed;

			Vector3 velocidadeAntiga = selfRigidbody.velocity;
			Vector3 mudancaVelocidade = velocidadeNova - velocidadeAntiga;
			mudancaVelocidade.x = Mathf.Clamp (mudancaVelocidade.x, -speed, speed);
			mudancaVelocidade.y = 0;
			mudancaVelocidade.z = Mathf.Clamp (mudancaVelocidade.z, -speed, speed);
			selfRigidbody.AddForce (mudancaVelocidade, ForceMode.VelocityChange);
		}

		selfRigidbody.AddForce (new Vector3(0, - gravity * 0.8f, 0));
		Debug.Log (grounded.ToString ());
		grounded = false;
	
	}

	float JumpVelocity () {
		return Mathf.Clamp(Mathf.Sqrt (2 * jumpHeight * gravity), -maxFallSpeed, maxFallSpeed);
	}

	/*void Death() {
		GameObject deathMenu = GameObject.FindGameObjectWithTag ("DeathMenu");
		deathMenu.GetComponent<DeathMenu> ().show = true;
	}*/

	bool CheckIfGrounded() {
		rayLenght = selfCollider.height / 2 - selfCollider.radius + 2 * selfCollider.radius / Mathf.Sqrt (3);
		check = Physics.Raycast (transform.position, Vector3.down, out hit, rayLenght);
		if (check) {
			isJumping = false;
			gravity = 10.0f;
		}
		//Debug.Log (check.ToString ());
		//Debug.Log (hit.distance.ToString ());

		return check;
	}

	void CheckIfWatered() {
		if (Physics.Raycast (transform.position, Vector3.up, selfCollider.height / 6, 4)) {
			Debug.Log ("True");
			//Death ();
		}
		return;
	}

	void OnCollisionEnter (Collision bateu) {
		if (bateu.collider.tag == "Terrain") {
			selfAudioSource.clip = fallAudio;
			selfAudioSource.Play ();
			//if (Mathf.Abs (bateu.relativeVelocity.y) > 10)
				//Death ();
		}
	}

}

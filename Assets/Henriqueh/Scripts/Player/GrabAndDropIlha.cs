﻿using UnityEngine;
using System.Collections;

public class GrabAndDropIlha : MonoBehaviour {
	public float rayLenght;
	GameObject grabbedObject;

	bool CheckIfCanGrab (GameObject thing) {
		if (thing.GetComponent<Rigidbody>() == null)
			return false;
		else
			return true;
	}

	GameObject GetMouseOnObject () {
		RaycastHit hit;
		Vector3 position = gameObject.transform.position;
		if (Physics.Raycast (position, Camera.main.transform.forward, out hit, rayLenght))
			return hit.collider.gameObject;
		return null;
	}

	void GrabObject (GameObject grabObject) {
		if (grabObject == null || CheckIfCanGrab(grabObject) == false)
			return;
		grabbedObject = grabObject;
		//grabbedObject.transform.parent = gameObject.transform;
		//grabbedObject.transform.position += new Vector3 (0f, 1.0f, grabbedObject.GetComponent<Collider>().bounds.size.magnitude);
		grabbedObject.GetComponent<Rigidbody> ().freezeRotation = true;
		grabbedObject.GetComponent<Rigidbody> ().isKinematic = true;
		grabbedObject.GetComponent<Rigidbody> ().useGravity = false;
	}

	void DropObject () {
		if (grabbedObject == null)
			return;
		//grabbedObject.transform.parent = null;
		grabbedObject.GetComponent<Rigidbody> ().freezeRotation = false;
		grabbedObject.GetComponent<Rigidbody> ().isKinematic = false;
		grabbedObject.GetComponent<Rigidbody> ().useGravity = true;
		grabbedObject = null;
	}

	void Update () {
		if (Input.GetButtonDown ("Interact")) {
			if (grabbedObject == null)
				GrabObject (GetMouseOnObject ());
			else
				DropObject ();
		}

		if (grabbedObject != null) {
			grabbedObject.transform.position = transform.position + grabbedObject.GetComponent<Collider> ().bounds.size.magnitude * transform.forward;
			grabbedObject.transform.rotation= transform.rotation;
		}
	}

}

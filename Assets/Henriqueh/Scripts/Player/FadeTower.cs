﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FadeTower : MonoBehaviour {

	public float fadeSpeed = 1.0f;
	public float fadeOutSpeed;
	public bool shouldFadeOut = false;
	private bool isStarting = true;
	public int SceneNumber;

	RawImage image;

	void Awake(){
		image = GetComponent<RawImage> ();
		image.rectTransform.localScale = new Vector3 (Screen.width, Screen.height, 1);
	}

	void Update () {
		if (isStarting)
			StartScene ();
		if (shouldFadeOut)
			FadeToBlack ();
	}

	void FadeToClear () {
		image.color = Color.Lerp (image.color, Color.clear, fadeSpeed * Time.deltaTime);
	}

	void FadeToBlack () {
		image.enabled = true;
		image.color = Color.Lerp (image.color, Color.white, fadeOutSpeed * Time.deltaTime);
		if (image.color.a >= 0.97f) {
			SceneManager.LoadScene (SceneNumber);
		}
	}

	void StartScene () {
		FadeToClear ();

		if (image.color.a <= 0.05f) {
			//image.color = Color.clear;
			image.enabled = false;
			isStarting = false;
		}
	}

	public void EndScene(int SceneNumber) {
		image.enabled = true;

		while (image.color.a <= 0.98f)
			FadeToBlack ();

		if (image.color.a >= 0.97f) {
			SceneManager.LoadScene (SceneNumber);
		}
	}
}

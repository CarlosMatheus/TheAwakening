﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CameraScriptRotate : MonoBehaviour {

	public GameObject camera1, camera2, player;

	float t=0f, deley=0, tFinal=0;
	public float fadeInTime, fadeOutTime;

	bool aux=true, aux2=true, aux3=true, aux4=true, aux5 = true;
	public bool terminou=false;
	public GameObject camerafinal;

	// Use this for initialization
	void Start () 
	{

	}

	// Update is called once per frame
	void Update () {

		t += Time.deltaTime;

		for (; aux; aux = false) {
			camera1.GetComponent<Camera> ().enabled = false;
			camera1.SetActive (false);
		}
		/*
		if (aux3 && deley<1)
		{
			deley+=Time.deltaTime;
			camera1.transform.eulerAngles = new Vector3 (38,0,0);
		}*/
		if (t<=10)
			camera2.transform.Rotate (-4 * Time.deltaTime, 0, 0);

		if (t>15 && aux2)
		{

			camera2.GetComponent<Camera>().enabled = false;
			camera2.SetActive (false);
			camera1.SetActive (true);
			camera1.GetComponent<Camera>().enabled = true;


			camera1.transform.eulerAngles = new Vector3 (38,0,0);
			print("ṕassando para a camera 1");

			player.transform.position = new Vector3 (0,-1.353734f,0);

			aux2 = false;
			aux3 = false;

			//camera2.SetActive (false);
			//Destroy(camera2);
		}


		if (terminou)
			final();


	}


	//funcao para terminar a fase
	void final()
	{
		tFinal += Time.deltaTime;

		camera1.GetComponent<Camera> ().enabled = false;
		camera1.SetActive (false);
		camera2.SetActive (true);
		camera2.GetComponent<Camera> ().enabled = true;

		camera2.transform.position = new Vector3 (668, 668, -1464);
		camera2.transform.localEulerAngles=new Vector3 (-23.2f, 192.66f, 0.8f);


		camerafinal.GetComponent<fade> ().fadeInTime = fadeInTime;
		camerafinal.GetComponent<fade> ().fadeOutTime = fadeOutTime;

		if(aux4)
		{
			aux4 = false;
			camerafinal.GetComponent<fade> ().aux = true;
		}
		if (aux5) {
			aux5 = false;
			camerafinal.GetComponent<fade> ().aux2 = true;
			camerafinal.GetComponent<fade> ().shouldFade = true;
		}
		if (tFinal > 40) 
		{
		camerafinal.GetComponent<fade> ().shouldFadeOut = true;

		
			if (tFinal > 50)
			{
				SceneManager.LoadScene (10);
			}

		}
	}

}

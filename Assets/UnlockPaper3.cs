﻿using UnityEngine;
using System.Collections;

public class UnlockPaper3 : MonoBehaviour {

	public GameObject jogador;
	public GameObject barreira;

	Vector3 p_jogador = new Vector3(0,0,0);

	// Use this for initialization

	
	// Update is called once per frame
	void Update () {
		p_jogador = jogador.transform.position;
		float distancia;
		distancia = Mathf.Sqrt( Mathf.Pow( (p_jogador.x - transform.position.x), 2f) + Mathf.Pow( (p_jogador.y - transform.position.y), 2f) + Mathf.Pow((p_jogador.z - transform.position.z), 2f));

		if(distancia <= 3f)
		{
			Destroy (barreira);
		}
	

	}
}

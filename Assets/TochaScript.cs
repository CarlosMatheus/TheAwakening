﻿using UnityEngine;
using System.Collections;

public class TochaScript : MonoBehaviour {

	public float delta= 0.3f, delta2;
	int cont =1;

	public Transform Player;
	public Transform TochaVerde;
	public Transform TochaVermelha;
	public Transform TochaAmarela;
	public Transform TochaAzul;
	public Transform FogoAzul, FogoAmarela, FogoVermelha, FogoVerde;
	public AudioSource som;
	public AudioSource vitoria;


	public AudioSource achou1,achou2,achou3,achou4;
	bool aux1=true,aux2=false,aux3=false,aux4=false;

	public GameObject monstro, ScriptDeFade;

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
	
		tele ();

		if 
			(
			(	   Mathf.Abs (Player.position.x - TochaVerde.position.x) <= delta2
			    && Mathf.Abs (Player.position.y - TochaVerde.position.y) <= delta2
			    && Mathf.Abs (Player.position.z - TochaVerde.position.z) <= delta2
			) ||
			(	   Mathf.Abs (Player.position.x - TochaVermelha.position.x) <= delta2
			    && Mathf.Abs (Player.position.y - TochaVermelha.position.y) <= delta2
			    && Mathf.Abs (Player.position.z - TochaVermelha.position.z) <= delta2
			) ||
			(	   Mathf.Abs (Player.position.x - TochaAmarela.position.x) <= delta2
			    && Mathf.Abs (Player.position.y - TochaAmarela.position.y) <= delta2
			    && Mathf.Abs (Player.position.z - TochaAmarela.position.z) <= delta2
			) ||
			(	   Mathf.Abs (Player.position.x - TochaAzul.position.x) <= delta2
			    && Mathf.Abs (Player.position.y - TochaAzul.position.y) <= delta2
			    && Mathf.Abs (Player.position.z - TochaAzul.position.z) <= delta2
			)
			) 
		{
			if (aux1) 
			{
				aux1 = false;
				achou1.Play ();
				print ("sound 1 played");
				monstro.GetComponent<SpawScript> ().time = monstro.GetComponent<SpawScript> ().time2;

			}
			if (aux2) 
			{
				aux2 = false;
				achou2.Play ();
				print ("sound 2 played");
				monstro.GetComponent<SpawScript> ().time = monstro.GetComponent<SpawScript> ().time3;
			}
			if (aux3) 
			{
				aux3 = false;
				achou3.Play ();
				print ("sound 3 played");
				monstro.GetComponent<SpawScript> ().time = monstro.GetComponent<SpawScript> ().time4;
			}
			if (aux4) 
			{
				aux4 = false;
				achou4.Play ();
				print ("sound 4 played");
			}
		}
	}

	void tele()
	{
		if (
			(
			Mathf.Abs (Player.position.x - TochaVerde.position.x) <= delta
			&& 
			Mathf.Abs (Player.position.y - TochaVerde.position.y) <= delta
			&& 
			Mathf.Abs (Player.position.z - TochaVerde.position.z) <= delta
			) ||

			(
			Mathf.Abs (Player.position.x - TochaVermelha.position.x) <= delta
			&& Mathf.Abs (Player.position.y - TochaVermelha.position.y) <= delta
			&& Mathf.Abs (Player.position.z - TochaVermelha.position.z) <= delta
			) ||

			(
			Mathf.Abs (Player.position.x - TochaAmarela.position.x) <= delta
			&& Mathf.Abs (Player.position.y - TochaAmarela.position.y) <= delta
			&& Mathf.Abs (Player.position.z - TochaAmarela.position.z) <= delta
			) ||

			(
			Mathf.Abs (Player.position.x - TochaAzul.position.x) <= delta
			&& Mathf.Abs (Player.position.y - TochaAzul.position.y) <= delta
			&& Mathf.Abs (Player.position.z - TochaAzul.position.z) <= delta
			)) 
		{

			som.Play ();
			Player.position = new Vector3 (0, -0.7f, 0);

			//will change the position of the torches
			trocarTocha ();

			//cont is now increased by one 
			switch (cont) 
			{
			case 2:
				aux2 = true;
				break;
			case 3:
				aux3 = true;
				break;
			case 4:
				aux4 = true;
				break;
			}
				
		}
	}
	void trocarTocha()
	{
		switch (cont)
		{
		case 1:
			Vector3 posicao1 = new Vector3 (TochaAzul.position.x, TochaAzul.position.y, TochaAzul.position.z);
			TochaAzul.position = TochaAmarela.position;
			TochaAmarela.position = posicao1;
			FogoAzul.position = new Vector3 (1000, 1000, 1000);
			cont++;
			break;
		
		case 2:
			Vector3 posicao2 = new Vector3 (TochaAmarela.position.x, TochaAmarela.position.y, TochaAmarela.position.z);
			TochaAmarela.position = TochaVermelha.position;
			TochaVermelha.position = posicao2;
			FogoAmarela.position = new Vector3 (1000, 1000, 1000);
			cont++;
			break;
		
		case 3:
			Vector3 posicao3 = new Vector3 (TochaVermelha.position.x, TochaVermelha.position.y, TochaVermelha.position.z);
			TochaVermelha.position = TochaVerde.position;
			TochaVerde.position = posicao3;
			FogoVermelha.position = new Vector3 (1000, 1000, 1000);
			cont++;
			break;
				
		case 4:

			//Player.position = new Vector3 (300, 10000, 0);
			FogoVerde.position = new Vector3 (1000, 1000, 1000);
			vitoria.Play ();
			ScriptDeFade.GetComponent<CameraScriptRotate> ().terminou = true;
			break;

		}

	}

}

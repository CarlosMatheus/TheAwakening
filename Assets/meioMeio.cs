﻿using UnityEngine;
using System.Collections;

public class meioMeio : MonoBehaviour {

	float t=0;
	bool aberto=false, aux=true;
	bool okey=false;
	public AudioSource fechando;
	public AudioSource abrindo;
	public AudioSource correntes;

	public Transform quina1,quina2,quina3,quina4;
	public int inicio = 1;

	int mod =0;

	Vector3 posicao1,posicao2,posicao3,posicao4;
	Vector3 posicao1Ini,posicao2Ini,posicao3Ini,posicao4Ini;

	// Use this for initialization
	void Start () 
	{
		
	}

	// Update is called once per frame
	void Update () 
	{
		t += Time.deltaTime;

		if (t > 1 && aux) {
			posicao1Ini = quina1.position;
			posicao2Ini = quina2.position;
			posicao3Ini = quina3.position;
			posicao4Ini = quina4.position;

			print ("posicao");
			aux = false;
		}


		if(aberto && okey)
			abrir ();
		
		if(!aberto && okey)
			fechar ();
		
		if (aberto == false && t >= 60) 
		{ 
			abrindo.Play ();
			t = 0;
			aberto = true;
			okey = true;

			posicao1 = quina1.position;
			posicao2 = quina2.position;
			posicao3 = quina3.position;
			posicao4 = quina4.position;
		}


		if (aberto == true && t >= 60) 
		{

			fechando.Play ();
			correntes.Play ();
			t = 0;
			aberto = false;
			okey = true;
		}

	}
	void abrir()
	{
		switch (inicio) {

		case 1:
			if (t >= 1) {
				okey = false;
			}
		
			print ("Abriu");
			quina1.position = new Vector3 (posicao1.x, posicao1.y + 3f * Mathf.Cos (Mathf.PI * 0.5f * t + Mathf.PI * 0.5f), posicao1.z);
			break;

		case 2:
			if (t >= 1) {
				okey = false;
			}

			print ("Abriu");
			quina2.position = new Vector3 (posicao2.x, posicao2.y + 3f * Mathf.Cos (Mathf.PI * 0.5f * t + Mathf.PI * 0.5f), posicao2.z);
			break;

		case 3:
			if (t >= 1) {
				okey = false;
			}

			print ("Abriu");
			quina3.position = new Vector3 (posicao3.x, posicao3.y + 3f * Mathf.Cos (Mathf.PI * 0.5f * t + Mathf.PI * 0.5f), posicao3.z);
			break;

		case 4:
			if (t >= 1) {
				okey = false;
			}

			print ("Abriu");
			quina4.position = new Vector3 (posicao4.x, posicao4.y + 3f * Mathf.Cos (Mathf.PI * 0.5f * t + Mathf.PI * 0.5f), posicao4.z);
			break;
		}
	}

	void fechar()
	{

		switch (inicio) {
		case 1:
			print ("Abriu");
			quina1.position = new Vector3 (posicao1.x, posicao1.y + 3f * Mathf.Cos (Mathf.PI * 0.5f * t + Mathf.PI),posicao1.z);

			if (t >= 1) 
			{
				okey = false;
				inicio = 2;

				quina1.position = new Vector3 (quina1.position.x,-1.133739f, quina1.position.z);
			}
			break;

		case 2:
			print ("Abriu");
			quina2.position = new Vector3 (posicao2.x, posicao2.y + 3f * Mathf.Cos (Mathf.PI * 0.5f * t + Mathf.PI),posicao2.z);

			if (t >= 1) 
			{
				okey = false;
				inicio = 3;

				quina2.position = new Vector3 (quina2.position.x,-1.133739f, quina2.position.z);
			}
			break;

		case 3:
			print ("Abriu");
			quina3.position = new Vector3 (posicao3.x, posicao3.y + 3f * Mathf.Cos (Mathf.PI * 0.5f * t + Mathf.PI),posicao3.z);

			if (t >= 1) 
			{
				okey = false;
				inicio = 4;

				quina3.position = new Vector3 (quina3.position.x,-1.133739f, quina3.position.z);
			}
			break;

		case 4:
			print ("Abriu");
			quina4.position = new Vector3 (posicao4.x, posicao4.y + 3f * Mathf.Cos (Mathf.PI * 0.5f * t + Mathf.PI),posicao4.z);

			if (t >= 1) 
			{
				okey = false;
				inicio = 1;

				quina4.position = new Vector3 (quina4.position.x,-1.133739f, quina4.position.z);
			}
			break;
		}
			
	}


}

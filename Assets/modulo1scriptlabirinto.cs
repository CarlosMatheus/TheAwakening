﻿using UnityEngine;
using System.Collections;


public class modulo1scriptlabirinto : MonoBehaviour {

	int inicio=1;
	float t=0;
	public Transform player;
	public Transform TochaVerde;
	public Transform TochaVermelha;
	public Transform TochaAmarela;
	public Transform TochaAzul;

	public Transform Luzes;

	public Transform Modulo1;
	public Transform Modulo2;
	public Transform Modulo3;
	public Transform Modulo4;

	bool deley=true, aux=false;

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
		t += Time.deltaTime;

		if (deley && t>2) {
			t = 0;
			deley = false;
			print ("deley");
		}

		if (t>2) {
			aux = true;
		}

			
		if (t >= 120) 
		{
			trocar ();

			t = 0;

			if (aux &&(player.position.x > 0 && player.position.z > 0) && !(Mathf.Abs (player.position.x) < 2 && Mathf.Abs (player.position.z) < 2 
				&& !((Mathf.Abs (player.position.x) >1 && Mathf.Abs (player.position.x) < 2) &&
					(Mathf.Abs (player.position.z) >1 && Mathf.Abs (player.position.z) < 2)) )) 
			{
				print ("mod");
				player.position = new Vector3 (player.position.x - 14f, player.position.y, player.position.z);
				aux = false;
			}

			if (aux && (player.position.x < 0 && player.position.z > 0) && !(Mathf.Abs (player.position.x) < 2 && Mathf.Abs (player.position.z) < 2 
				&& !((Mathf.Abs (player.position.x) >1 && Mathf.Abs (player.position.x) < 2) &&
					(Mathf.Abs (player.position.z) >1 && Mathf.Abs (player.position.z) < 2)) )) 
			{
				print ("mod");
				player.position = new Vector3 (player.position.x, player.position.y, player.position.z - 14f);
				aux = false;
			}

			if (aux && (player.position.x < 0 && player.position.z < 0) && !(Mathf.Abs (player.position.x) < 2 && Mathf.Abs (player.position.z) < 2 
				&& !((Mathf.Abs (player.position.x) >1 && Mathf.Abs (player.position.x) < 2) &&
					(Mathf.Abs (player.position.z) >1 && Mathf.Abs (player.position.z) < 2)) )) 
			{
				print ("mod");
				player.position = new Vector3 (player.position.x + 14f, player.position.y, player.position.z);
				aux = false;
			}

			if (aux && (player.position.x > 0 && player.position.z < 0) && !(Mathf.Abs (player.position.x) < 2 && Mathf.Abs (player.position.z) < 2 
				&& !((Mathf.Abs (player.position.x) >1 && Mathf.Abs (player.position.x) < 2) &&
					(Mathf.Abs (player.position.z) >1 && Mathf.Abs (player.position.z) < 2)) )) 
			{
				print ("mod");
				player.position = new Vector3 (player.position.x, player.position.y, player.position.z + 14f);
				aux = false;
			}

		}


	}

	void trocar()
	{
		Vector3 posicao1 = new Vector3 (Modulo1.position.x,Modulo1.position.y,Modulo1.position.z);

		trocarTocha ();

		Modulo1.position = Modulo2.position;
		Modulo2.position = Modulo3.position;
		Modulo3.position = Modulo4.position;
		Modulo4.position = posicao1;
	}

	void trocarTocha ()
	{
		Vector3 posicao2 = TochaVerde.position;

		TochaVerde.position = TochaVermelha.position;
		TochaVermelha.position = TochaAzul.position;
		TochaAzul.position = TochaAmarela.position;
		TochaAmarela.position= posicao2;

	}

}
# The Awakening


<img src="https://media.giphy.com/media/8PjJ8tyUVcfucEgZCf/giphy.gif" width="320" height="180" />
<img src="https://media.giphy.com/media/F0OuZdftYfdT6Am2Cg/giphy.gif" width="320" height="180" />

<img src="https://media.giphy.com/media/fMB1tSDbahPAmjWNKD/giphy.gif" width="320" height="180" />
<img src="https://media.giphy.com/media/lpfqBwQ2eb4Kqsc1Uk/giphy.gif" width="320" height="180" />


“You awake up alone in a stunning island and can’t remember anything of what happened. Now the only thing left is to solve a number of puzzles that you find that allows you to go deeper in your mind" 


Solve the challenges and discovery the mystery of that island!

## Playing the game

You can download and play The Awakening on its [itch.io page](https://carlos-matheus.itch.io/the-awakening).

## About


The Awakening was developed by three freshman students of Instituto tecnológico de Aeronáutica - [ITA](http://www.ita.br/).

The project is a First Person Puzzle game. Is was made with Unity Engine.

It was originally developed to compete CIG 2016, in which won the second place. CIG is the Intituto Tecnologico de Aeronautica - [ITA](http://www.ita.br/) - internal game development competition.


## Notes about contributing

If any substantial change is made, please, contact any of the authors.

## Authors

* **Carlos Matheus Barros da Silva** - [CarlosMatheus](https://github.com/CarlosMatheus)
* **Felipe Vieira Coimbra** - [FelipeCoimbra](https://github.com/FelipeCoimbra)
* **Luiz Henrique Aguiar de Lima Alves** - [HikkusT](https://github.com/HikkusT)

